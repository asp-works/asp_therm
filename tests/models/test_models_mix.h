/**
 * asp_therm - implementation of real gas equations of state
 *
 *
 * Copyright (c) 2020 Mishutinski Yurii
 *
 * This library is distributed under the MIT License.
 * See LICENSE file in the project root for full license information.
 */
#ifndef TEST_MODELS_MIX_H_
#define TEST_MODELS_MIX_H_

int test_models();
int test_models_mix();

#endif  // !TEST_MODELS_MIX_H_
